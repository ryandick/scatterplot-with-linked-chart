'use strict';
angular.module( 'scatterDemo' )
	.controller( 'MainCtrl', function ( $scope, $http ) {
		//	angular scope variables
		$scope.vd = [ ];
		$scope.vdCount = 0;
		$scope.rawData = null;
		//	get data for view
		$http.get( 'data.json' ).success( function ( d ) {
			$scope.rawData = d;
			var _data = [ ];
			/////////////////////////////////
			//	Table Data Processing// //
			/////////////////////////////////
			//	function for processing data for each grouping
			var getGroupData = function ( group ) {
				//	iterate each data item in group
				for ( var i = group.values.length - 1; i >= 0; i-- ) {
					//	index
					$scope.vdCount++;
					//	current item
					var item = group.values[ i ];
					//	set index
					item.id = $scope.vdCount;
					//	set group property
					item.group = group.key;
					//	push into array
					_data.push( item );
					//	if we have got to the end of the collection
					if ( i == 0 ) {
						return _data;
					}
				}
			};
			//	iterate groupings
			for ( var i = d.length - 1; i >= 0; i-- ) {
				var group = d[ i ];
				//	process each groups dataset
				getGroupData( group );
			}
			//	set scoped view data
			$scope.vd = _data;
			///////////////////////
			//	CHART Setup// //
			///////////////////////
			//	add new graph to controller
			nv.addGraph( function ( ) {
				//	use scatterplot plust line chart and set options
				var chart = nv.models.scatterPlusLineChart( )
					.showDistX( true )
					.showDistY( true )
					.useVoronoi( false )
					.interactive( true )
					.tooltipContent( function ( key, x, y ) {
						return '<div style="padding:5px;"><h5> <b>Group: </b><i>' + key + '</i><br /><b>Item: </b> <i class="tooltipItem"></i></h5><h5> <b>x-value: </b>' + x + '<br /><b>y-value: </b>' + y + '</h5></div>';
					} )
					.color( d3.scale.category10( ).range( ) );
				//	axis settings
				chart.xAxis.axisLabel( 'X Axis' ).tickFormat( d3.format( '.0f' ) );
				chart.yAxis.axisLabel( 'Y Axis' ).tickFormat( d3.format( '.0f' ) );
				//	insert svg into div with d3
				d3.select( '#chart' )
					.append( 'svg:svg' )
					.datum( $scope.rawData )
					.transition( ).duration( 500 )
					.call( chart );
				//	refresh on wndow resize
				nv.utils.windowResize( chart.update );
				//	bind to event when mouse over datapoint
				chart.scatter.dispatch.on( 'elementMouseover', function ( e ) {
					// 	set tooltip item name
					$( '.tooltipItem' ).text( e.point.key );
					//	iterate each row in data grid looking for corresponding key to e.point
					$( '.vdvRow' ).each( function ( index, row ) {
						if ( ( $( row ).children( '.vdv_series' ).data( 'key' ) == e.point.key ) ) {
							//	on finding the right row reset active row
							$( 'tr' ).removeClass( 'vdvactive' );
							//	add active class to row
							$( row ).addClass( 'vdvactive' );
							//	animate scrolling into view
							$( '.tableContain' ).scrollTo( row, 250 );
						}
					} );
				} );
				//	return constructor
				return chart;
			} );
		} );
	} );
