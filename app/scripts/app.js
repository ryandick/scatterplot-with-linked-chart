'use strict';
angular.module( 'scatterDemo', [ 'd3' ] ).config( function ( $routeProvider ) {
	//	routing
	$routeProvider.when( '/', {
		templateUrl: 'views/main.html',
		controller: 'MainCtrl'
	} ).otherwise( {
		redirectTo: '/'
	} );
} );
